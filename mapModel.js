var mymap = L.map('mapid').fitWorld( [40.712, -74.227], [40.774, -74.125]);
var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png', {
    minZoom: 2,
    maxZoom: 18,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);
var greenIcon = L.icon({
    iconUrl: 'images/man-working-on-a-laptop-from-side-view.png',
    iconSize:     [25  , 25], // size of the icon
    iconAnchor:   [10, 10], // point of the icon which will correspond to marker's location
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});
var marker = L.marker([32.083669, 34.803790],{icon: greenIcon}).addTo(mymap);
var marker = L.marker([40.762632, -73.973826]).addTo(mymap);