// This example uses MediaRecorder to record from a live audio stream,
// and uses the resulting blob as a source for an audio element.
//
// The relevant functions in use are:
//
// navigator.mediaDevices.getUserMedia -> to get audio stream from microphone
// MediaRecorder (constructor) -> create MediaRecorder instance for a stream
// MediaRecorder.ondataavailable -> event to listen to when the recording is ready
// MediaRecorder.start -> start recording
// MediaRecorder.stop -> stop recording (this will generate a blob of data)
// URL.createObjectURL -> to create a URL from a blob, which we can use as audio src

window.recorder = null;
window.isRecording = false;
window.audioChunks = [];
window.onload = function () {
    // get audio stream from user's mic
    navigator.mediaDevices.getUserMedia({
        audio: true
    })
        .then(function (stream) {
            recorder = new MediaRecorder(stream);
            document.addEventListener('keydown', reportKeyEvent.bind(window));
            document.addEventListener('keyup', reportKeyReleaseEvent.bind(window));
            window.recorder.addEventListener("dataavailable", collectAudio.bind(window));
            window.recorder.addEventListener("stop", () => {
                const audioBlob = new Blob(window.audioChunks);
                let reader = new FileReader();
                reader.readAsDataURL(audioBlob);
                reader.onload = function () {
                    let b64 = reader.result;
                    console.log(b64);
                };
                // const audio = new Audio("data:audio/x-wav;base64; " + URL.createObjectURL(audioBlob));
                // audio.play();
                window.audioChunks = [];
            });
        });
};

function collectAudio(e) {
    window.audioChunks.push(e.data);
}

function reportKeyEvent(e) {
    //--- Was a Ctrl-Shift combo pressed?
    if (e.ctrlKey && e.shiftKey && !window.isRecording) {
        console.log("start");
        window.recorder.start();
        window.isRecording = true;
    }
    e.stopPropagation();
}


function reportKeyReleaseEvent(e) {
    if (!e.ctrlKey && !e.shiftKey && window.isRecording) {
        console.log("stop");
        window.recorder.stop();
        window.isRecording = false;
    }
    e.stopPropagation();
}